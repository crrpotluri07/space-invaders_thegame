#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb  9 19:19:05 2020

@author: chaitanyapotluri
"""

import pygame

#INITIALIZE pygame - 
#if you are on mac:
#   you'd get a pop up to enable the access of keystrokes to ssh 
#   as the game needs inputs from the keyboard, enable the access
pygame.init()

#create the screen window
screen = pygame.display.set_mode((800, 600))

#Title and Icon
pygame.display.set_caption("Space Invaders")
icon = pygame.image.load("/Users/chaitanyapotluri/Desktop/Chaitanya/GIT/spaceinvaders_thegame/spaceship.png")
pygame.display.set_icon(icon)

#player
playerImg = pygame.image.load("/Users/chaitanyapotluri/Desktop/Chaitanya/GIT/spaceinvaders_thegame/spaceship.png")
player_X = 360
player_Y = 470

#player functionality
def player():
    screen.blit(playerImg, (player_X, player_Y))

#start the screen
running = True
while running:
    screen.fill((0,0,0))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
            exit()
    player()
    pygame.display.update()
    
            



